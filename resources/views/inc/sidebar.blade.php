<div class="container-fluid">
   <div class="row">
     <nav class="col-md-2 d-none d-md-block bg-light sidebar">
       <div class="sidebar-sticky">
         <ul class="nav flex-column">
           <li class="nav-item">
             <a class="nav-link" href="/administration/patient">
               <span data-feather="file"></span>
               Patient
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="/administration/medication">
               <span data-feather="shopping-cart"></span>
               Medication
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="/administration/dosage">
               <span data-feather="users"></span>
               Dosage
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="/administration/time">
               <span data-feather="bar-chart-2"></span>
               Time
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="/administration/route">
               <span data-feather="layers"></span>
               Route
             </a>
           </li>
           <li class="nav-item">
             <a class="nav-link" href="/administration/documentation">
               <span data-feather="layers"></span>
               Documentation
             </a>
           </li>
         </ul>
       </div>
</nav>
