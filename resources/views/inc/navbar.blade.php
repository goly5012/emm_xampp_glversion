<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="/">Home</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item">
        <a class="nav-link" href="/administration">Administration</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/medications">Medications</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/search">Database Search</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/dsearch">Dynamic Search</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/patients">Patient List</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="/findpatients">Patient Search</a>
      </li>
    </ul>
  </div>
</nav>
