<!DOCTYPE html>
<html>
 <head>
  <title>Dynamic Database Search</title>
  <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/css/select2.min.css" rel="stylesheet" />
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.0/jquery.min.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" />
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.3/js/select2.min.js"></script>
  <style type="text/css">
   .box{
    width:600px;
    margin:0 auto;
    border:1px solid #ccc;
   }
  </style>
 </head>
 <body>
  <br />
  <div class="container box">
   <h3 align="center">Dynamic Search of Database</h3><br />
   <div class="form-group">
    <select name="generic_name" id="generic_name" class="form-control input-lg dynamic" data-dependent="brand">
     <option value="">Select Generic Name</option>
     @foreach($generic_list as $generic_name)
     <option value="{{ $generic_name->generic_name}}">{{ $generic_name->generic_name }}</option>
     @endforeach
    </select>
   </div>
   <br />
   <div class="form-group">
    <select name="brand" id="brand" class="form-control input-lg dynamic" data-dependent="strength">
     <option value="">Select Brand</option>
    </select>
   </div>
   <br />
   <div class="form-group">
    <select name="strength" id="strength" class="form-control input-lg">
     <option value="">Select Strength</option>
    </select>
   </div>
   {{ csrf_field() }}
   <br />
   <br />
  </div>
 </body>
</html>


<script type="text/javascript">

      $("#generic_name").select2({
            placeholder: "Search for a Generic Name",
            allowClear: true
        });
</script>

<script>
$(document).ready(function(){

 $('.dynamic').change(function(){
  if($(this).val() != '')
  {
   var select = $(this).attr("id");
   var value = $(this).val();
   var dependent = $(this).data('dependent');
   var _token = $('input[name="_token"]').val();
   $.ajax({
    url:"{{ route('dynamicdependent.fetch') }}",
    method:"POST",
    data:{select:select, value:value, _token:_token, dependent:dependent},
    success:function(result)
    {
     $('#'+dependent).html(result);
    }

   })
  }
 });

 $('#generic_name').change(function(){
  $('#brand').val('');
  $('#strength').val('');
 });

 $('#brand').change(function(){
  $('#strength').val('');
 });

 $(document).on('keyup', '#search', function(){
  var query = $(this).val();
  fetch_customer_data(query);
 });

});
</script>
