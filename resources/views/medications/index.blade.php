@extends('layouts.app')

@section('content')
        <div class ="row">
          <div class ="col-md-12">
            <br />
            <h3 align="center">Medication Database</h3>
            <br />
              <table class="table table-bordered">
                <tr>
                  <th>ID</th>
                  <th>Generic Name</th>
                  <th>Dosage</th>
                  <th>Indication</th>
                  <th>Brand</th>
                  <th>Strength</th>
                  <th>Route</th>
                  <th>Formulation</th>
                </tr>
                @foreach($medications as $row)
                  <tr>
                    <td>{{$row['id']}}</td>
                    <td>{{$row['generic_name']}}</td>
                    <td>{{$row['dosage']}}</td>
                    <td>{{$row['indication']}}</td>
                    <td>{{$row['brand']}}</td>
                    <td>{{$row['strength']}}</td>
                    <td>{{$row['route']}}</td>
                    <td>{{$row['formulation']}}</td>
                  </tr>
                @endforeach
        </table>
      </div>
    </div>
@endsection
