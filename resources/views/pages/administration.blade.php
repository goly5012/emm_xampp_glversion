@extends('layouts.adminapp')

@section('content')
          <h1>Administration Module</h1>
          <p>Select a Patient and perform the Six Rights of safe medication administration</p>
          @if(count($list) > 0)
            <ul class="list-group">
            @foreach($list as $list)
              <li class="list-group-item">{{$list}}</li>
            @endforeach
          </ul>
          @endif
@endsection
