@extends('layouts.adminapp')

@section('content')
          <h1>Check Time</h1>
          <p>Verify administration time matches order</p>
          <p>Verify before administering a PRN medication, specified time interval has passed</p>
@endsection
