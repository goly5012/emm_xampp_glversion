<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'PagesController@index');
Route::get('/administration', 'PagesController@administration');
Route::get('/administration/patient', 'PagesController@administration_patient');
Route::get('/administration/medication', 'PagesController@administration_medication');
Route::get('/administration/dosage', 'PagesController@administration_dosage');
Route::get('/administration/time', 'PagesController@administration_time');
Route::get('/administration/route', 'PagesController@administration_route');
Route::get('/administration/documentation', 'PagesController@administration_documentation');
Route::resource('medications','MedDatabaseController');
Route::get('/search', 'LiveSearch@index');
Route::get('/search/action', 'LiveSearch@action')->name('live_search.action');
Route::get('/dsearch', 'DynamicDependent@index');
Route::post('dsearch/fetch', 'DynamicDependent@fetch')->name('dynamicdependent.fetch');
Route::resource('patients','PatientController');
Route::get('/findpatients', 'PatientSearch@index');
Route::get('/findpatients/action', 'PatientSearch@action')->name('patient_search.action');
