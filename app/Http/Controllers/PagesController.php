<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PagesController extends Controller
{
    public function index() {
      return view('pages.index');
    }

    public function administration() {
      $data = array (
        'title' => 'Patients',
        'list' => ['P1', 'P2', 'P3']
      );
      return view('pages.administration')->with($data);
    }

    public function medications() {
      $data = array (
        'title' => 'Medications',
        'list' => ['A', 'B', 'C', 'D']
      );
      return view('pages.medications')->with($data);
    }

    public function administration_patient() {
      $data = array (
        'title' => 'Verify Patient',
      );
      return view('pages.patient');
    }

    public function administration_medication() {
      $data = array (
        'title' => 'Verify Medication',
      );
      return view('pages.medication');
    }

    public function administration_dosage() {
      $data = array (
        'title' => 'Verify Dosage',
      );
      return view('pages.dosage');
    }

    public function administration_time() {
      $data = array (
        'title' => 'Verify Time',
      );
      return view('pages.time');
    }

    public function administration_route() {
      $data = array (
        'title' => 'Verify Route',
      );
      return view('pages.route');
    }

    public function administration_documentation() {
      $data = array (
        'title' => 'Verify Documentation',
      );
      return view('pages.documentation');
    }
}
