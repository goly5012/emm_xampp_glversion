<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class DynamicDependent extends Controller
{
    function index()
    {
     $generic_list = DB::table('medications')
         ->groupBy('generic_name')
         ->get();
     return view('dynamic_dependent')->with('generic_list', $generic_list);
    }

    

    function fetch(Request $request)
    {
      $select = $request->get('select');
      $value = $request->get('value');
      $dependent = $request->get('dependent');
      $data = DB::table('medications')
      ->where($select, $value)
      ->groupBy($dependent)
      ->get();
      $output = '<option value="">Select '.ucfirst($dependent).'</option>';
      foreach($data as $row)
      {
        $output .= '<option value="'.$row->$dependent.'">'.$row->$dependent.'</option>';
      }
      echo $output;
    }
}

?>
