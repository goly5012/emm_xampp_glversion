<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;

class LiveSearch extends Controller
{
    function index()
    {
      return view('live_search');
    }

    function action(Request $request)
    {
     if($request->ajax())
     {
      $output = '';
      $query = $request->get('query');
      if($query != '')
      {
       $medications = DB::table('medications')
         ->where('generic_name', 'like', '%'.$query.'%')
         ->groupBy('generic_name')
         ->get();

      }
      else
      {
       $medications = DB::table('medications')
         ->orderBy('id', 'asc')
         ->get();
      }
      $total_row = $medications->count();
      if($total_row > 0)
      {
       foreach($medications as $row)
       {
        $output .= '
        <tr>
         <td>'.$row->generic_name.'</td>
         <td>'.$row->dosage.'</td>
         <td>'.$row->indication.'</td>
         <td>'.$row->brand.'</td>
         <td>'.$row->strength.'</td>
         <td>'.$row->route.'</td>
         <td>'.$row->formulation.'</td>
        </tr>
        ';
       }
      }
      else
      {
       $output = '
       <tr>
        <td align="center" colspan="5">No Data Found</td>
       </tr>
       ';
      }
      $medications = array(
       'table_data'  => $output,
       'total_data'  => $total_row
      );

      echo json_encode($medications);
     }
    }
}
